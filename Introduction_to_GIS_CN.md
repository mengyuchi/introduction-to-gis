---
marp: true
author: Yuchi Meng
size: 4:3
theme: default
style: @import url('https://unpkg.com/tailwindcss@^2/dist/utilities.min.css');

---

<!-- footer: 2022.12.21 -->

![bg auto blur:2px brightness:.9](./images/gut.jpg)

<!-- _color: "#F0F0F0" -->

# 桂林理工大学 - 测绘地理信息学院

## 应聘陈述报告

---

<!-- paginate: true -->

# 个人介绍 

## 孟禹弛 - 遥感与地理信息系统 - 博士在读

### 德国多特蒙德工业大学 - 空间规划学院
<br>
<div class="grid grid-cols-2 gap-1">
<div>

&emsp; &emsp; ![width:200px](./images/avatar.jpg)

</div>
<div>
<font size=5>

出生日期：1992年9月

籍贯：内蒙古鄂尔多斯市

政治面貌：中共党员

联系方式：15149616953

邮箱：mengyuchile@163.com

</font>
</div>

</div>
</div>

</br>

---

# 教育经历

- 2010.10 - 2014.06 • 中国矿业大学 • 计算机科学与技术
- 2014.09 - 2017.06 • 中国矿业大学 • 地图学与地理信息系统
- 2017.10 - 至   今 &emsp; • 多特蒙德工业大学 • 空间数据管理与建模

---

# 学术经历

<font size=5>

## **博士论文**: Land Subsidence Monitoring and Ecological Rehabilitation in Post Coal Mining City

## **学术报告**: Land Subsidence Monitoring in Coal Mining Area through the InSAR Technique - 第5届Dortmunder Konferenz 

## **研究方向**：雷达遥感，环境监测，地理信息应用等

</font>

---
<!-- _header: ![width:400px](./images/logo_blue.png) &emsp; &emsp; &emsp; &emsp; &ensp; ![width:400px](./images/cehui_logo.jpg) -->   

<!-- paginate: false -->

# 试讲：地理信息系统概论

孟禹弛

---

<!-- paginate: true -->

# 地理信息系统概论

## 课程大纲

1 | [空间数据](./Introduction_to_GIS_CN.pdf#16)

- 1.1 [概念与特征](./Introduction_to_GIS_CN.md#17)
- 1.2 [矢量数据结构](./Introduction_to_GIS_CN.md#24)


--- 

<!-- paginate: true -->

# *教学目的与课程重点:*

1. 空间数据模型的基本概念
2. 空间数据结构的基本概念
3. 矢量数据结构

---

# 1.1 | [概念与特征](./Introduction_to_GIS_CN.md#16)

---

# 现实世界 - GIS

<div class="grid grid-cols-2 gap-4">
<div>

![world](./images/world.png)

</div>

<div>

![world_to_gis](./images/world_representation.png)

</div>
</div>

---

# 地理空间 （Geographic Space）

<div class="grid grid-cols-2 gap-4">
<div>

是指地球表面及近地表空间，是地球上大气圈、水圈、生物圈、岩石圈和土壤圈交互作用的区域。

</div>

<div>

![world](./images/world.png)

</div>
</div>

---

# 空间实体 - 特征

- 对复杂地理事物和现象进行简化抽象得到的不可再分割的同类对象，就是地理空间实体。

空间实体具有4个基本特征，主要包括**空间位置**、**空间关系**、**时间特征**和**属性**。

![spatial_data](./images/spatial_data.png)

---
# 空间数据模型

![spatial data structure](./images/spatial%20data%20structure.jpg)

---

# 空间数据结构 - 定义

<div class="grid grid-cols-2 gap-4">
<div>

<font size=5>

**空间数据结构**(spatial data structure)是指对空间数据逻辑模型描述的数据组织关系和编排方式的具体实现，对地理信息系统中数据`存储`、`查询检索`和`应用分析`等操作处理的效率有着至关重要的影响。

</font>

</div>
<div>

![width:500px](./images/spatial_data_structure.png)

</div>
</div>

---

# 2种常用的空间数据结构:

- Vector 矢量

![vector](./images/data_model_vector.png)

- Raster 栅格

![raster](./images/data_model_raster.png)

--- 

# 1.2 | [矢量数据结构](./Introduction_to_GIS_CN.md#17)

## “位置明显，属性隐含”

**矢量数据结构**就是代表地图图形的各离散点平面坐标（x，y）的有序集合，主要用于表示地图图形元素几何数据之间及其与属性数据之间的相互关系。 

其坐标空间假定为连续空间，不必象栅格数据结构那样进行量化处理，因此矢量数据更能精确地确定实体的空间位置。

---

## 三种实体类型

<font size=5>

**点实体**：包括由单独一对x，y坐标定位的一切地理或制图实体。

**线实体**：线实体可以认为是由连续的直线段组成的曲线，用坐标串的集合（X1，Y1，X2，Y2……Xn，Yn）来记录；

**面实体**：在记录面实体时，通常通过记录面状地物的边界即条闭合的线来表达，因而有时也称为多边形数据。

</font>

![vector](./images/vector_types_of_geometry.png)

---

# 属性表

<div class="grid grid-cols-2 gap-4">
<div>

<font size=4>

 GIS中的属性表是用来存储地理要素的非空间信息，通常使用特殊标识符（unique identifer）将表格信息同地理要素链接。

地理数据库或表格文件包含一个地理元素集的信息，通常表现为：

- 每行代表一个地理元素，
- 每列代表一个元素属性。

属性值可以快速方便的查阅，检索，分析与符号化地理要素。
</font>


</div>
<div>

![width:500px](./images/attribute_table_structure.png)

</div>
</div>

---

# 属性表 - 数据类型

数据库中每列都可以是不同的数据类型

![data_types](./images/attribute_data_types.png)

## 一些基本数据类型:


<font size=4>

**值类数型 NUMERIC**: INTEGER (long int, short int) 
**浮点型数值 NUMERIC**: FLOAT (double, real) 
**字符型 STRING** (char, varchar, text) 
**日期类型 DATE/TIME** (date, time, year, timestamp)
**布尔类型 BOOLEAN** (0/1, true/false, yes/no) 

</font>

---
# 矢量数据来源

<br></br>
![bg height:300px](./images/vector_eg_a_gps_measure.png)
![bg height:300px](./images/vector_eg_b_coordinates.png)
![bg height:300px](./images/vector_eg_c_analysis.png)
![bg height:300px](./images/vector_eg_d_database1.png)
![bg height:300px](./images/vector_eg_e_database2.png)

<font size=3>

(A - GPS measurements, B - list of coordinates, C - digitizing tools e.g. raster to vector, D, E - existing databases)
</font>

---
<!-- paginate: false -->

![bg auto blur:0px brightness:.9](./images/thanks.jpg)

<!-- _color: "#000000" -->
# &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;试讲结束
# &emsp;&emsp;&emsp;&emsp;恳请各位老师批评指正

<br></br>

<br></br>