% 面试汇报的提纲

# 现实世界 - GIS

为了能够利用地理信息系统工具解决现实世界中的问题，首先必需将复杂的地理事物和现象简化和抽象到计算机中进行表示，处理和分析。这就需要对现实世界进行抽象建模，其结果就是空间数据模型。

# 地理空间

所谓的地理空间，我们大家都熟悉，就是我们的客观世界，地球表面的各种对象，或者现象。地球表面的5大圈层，他们之间的相互作用，复杂的物理过程，化学过程，生物过程等等。

# 空间实体 

当然这里的实体是指再某一个空间尺度下，在某种比例尺下，需要表达的最小单元，就是我们的空间实体。实体是一个相对的概念。

要刻画一个空间实体，我们在GIS中有这么几种特征来描述它。

客观对象的增长，消失，是有生命周期的。在认知的某一个时间中，它是存在的，过一段时间就会消息了。

# 

为了将复杂的地理事物和现象重载到计算机世界中，如何对其进行简化和抽象成为了GIS的基础性问题，而对现实世界进行抽象建模的结果就是数据模型，其包含着现实世界中空间实体及其相互联系的概念。

-  **概念数据模型** 是连接现实世界到概念世界的桥梁，是地理事物与现象的抽象概念的集合，是地理数据的语义解释。

- **逻辑数据模型** 是概念世界到计算机世界的桥梁（数据世界），其描述概念数据模型中实体及之间关系的逻辑结构，是系统抽象的中间层。通常所说空间数据模型的狭义就是逻辑数据模型。

- **物理数据模型** 是逻辑数据模型在计算机内部具体的存储形式和操作机制，即在物理磁盘上是如何存放和存取的，是系统抽象的最底层。

地理空间事物或现象，或者称实体，我们用3层模型进行表达。

# 栅格数据结构

由于栅格数据是按一定规则排列的，所以表示的实体位置关系是隐含在行号、列号之中的。

# 栅格数据结构用法

- 下面是一个用作道路数据底图的栅格。

- 下方的栅格便显示了高程，其中使用绿色显示较低的高程，红色、粉红色和白色像元则表示较高的高程。 

- 下方的示例是显示土地利用的分类栅格数据集。 

- 下方是一棵大型古树的数字图片，可用作城市地表图层的属性。